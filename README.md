# SpeechToText

This application helps to transcribe your audio files (wav and mp3) to text. It is based on the preceding work of John Singer and his <a href="https://singerlinks.com/2021/08/how-to-transcribe-a-podcast-to-text-for-free/">AudioText Tkinter application</a> shared on his private blog. 
My code extends this application and adds the choice to output a final txt file. Furthermore, it is possible to select whether the recognized text from the audio file should be further processed to try to correct punctuation and capitalization.

For both steps (audio recognition and punctuation/recasing) two different models are used, which are part of the open source vosk api. Feel free to get more information on <a href="https://alphacephei.com/vosk/"> the vosk website</a>. The punctuation and recasing predictor is based on the work of Benoit Favre. Please see the respective <a href="https://github.com/benob/recasepunc">github repository</a>.

To make the application work, please follow these steps:
1. Create a virtual environment with the necessary packages listed in the requirements file.
2. Download this repository
3. Download a suitable audio recognition model from the <a href="https://alphacephei.com/vosk/models">vosk page</a>.
4. Get the trained punctuation and recasing checkpoint dataset. Either from the <a href="https://alphacephei.com/vosk/models"> vosk page</a> or from <a href="https://github.com/benob/recasepunc"> Benoit Favre's github repository</a>
5. Install the ffmpeg package on your computer. If your using a windows computer and you are not familiar with this installation, feel free to have a look at the <a href="https://singerlinks.com/2021/07/speech-to-text-python-environment-setup-using-vosk/">step by step explanation</a> provided by John Singer. 

## Sreenshot
<img src="SpeechtoText.png">